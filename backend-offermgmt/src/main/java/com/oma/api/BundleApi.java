package com.oma.api;

import com.oma.entity.Bundle;
import com.oma.entity.Bundle_Mk;
import com.oma.repository.BundleRepository;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/bundle")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BundleApi {

    @Inject
    BundleRepository bundleRepository;

    @Inject
    EntityManager em;

    //-------------------Marketing level-------------------

    //Get the list of bundle
    @GET
    @Path("/mk")
    public List<Bundle_Mk> listBundle_Mk(){
        return Bundle_Mk.listAll();
    }

    //Get idbundle of the last record of bundle
    @GET
    @Path("/lastidbundle")
    @Transactional //use in CRUD operation
    public Object getLastIdbundle(){

        List q = em.createNativeQuery("SELECT idbundle FROM bundle ORDER BY idbundle DESC LIMIT 1")
                .getResultList();

        return q.get(0);
    }

    //Add new bundle
    @POST
    @Path("/mk")
    @Transactional
    public Response addBundle(Bundle_Mk bundle){
        bundle.persist();
        return Response.status(Response.Status.CREATED).entity(bundle).build();
    }



    //-------------------Config level-------------------

    //Get the list of bundle (All of table)
    @GET
    public List<Bundle> listBundle(){
        return Bundle.listAll();
    }

    //Update the bundle created
    @PUT
    @Path("{idbundle}")
    @Transactional
    public Response updateBundle(
            @PathParam("idbundle") Integer idbundle, Bundle bundle){
        Bundle bEntity = bundleRepository.update(idbundle, bundle);
        return Response.ok(bEntity).build();
    }
}
