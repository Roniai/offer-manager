package com.oma.api;

import com.oma.entity.BundleOfferelementaire;
import com.oma.tablepk.BundleOfferelementaireId;
import io.quarkus.panache.common.Parameters;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/bundleOfferelementaire")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BundleOfferelementaireApi {

    //-------------------Marketing level-------------------

    @Inject
    EntityManager em;

    //Add new idbundle, DA[], quantity[] in bundleofferelementaire
    @POST
    @Path("{idbundle}")
    @Transactional //use in CRUD operation
    public HashMap<Integer, Long> addBundleOfferelementaire(
            @PathParam("idbundle") Integer idbundle,
            HashMap<Integer, Long> listDAquantity) {

        for(Map.Entry DAquantity : listDAquantity.entrySet()){
            em.createNativeQuery("INSERT INTO bundleofferelementaire VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                .setParameter(1, idbundle)
                .setParameter(2, DAquantity.getKey())
                .setParameter(3, DAquantity.getValue())
                .setParameter(4, 0)
                .setParameter(5, 0)
                .setParameter(6, 0)
                .setParameter(7, 0)
                .setParameter(8, 0)
                .setParameter(9, 0)
                .setParameter(10, 0)
                .setParameter(11, 0)
                .setParameter(12, 0)
                .setParameter(13, 0)
                .setParameter(14, 0)
                .setParameter(15, 0)
                .setParameter(16, 0)
                .executeUpdate();
        }
        return listDAquantity;
    }

    //-------------------Config level-------------------

    //Get the list of bundleofferelementaire (All)
    @GET
    public List<BundleOfferelementaire> listBundleOfferelementaire(){ return BundleOfferelementaire.listAll();}

    //Update the bundleofferelementaire created
    //We don't have permission to change the "quantity"
    //But we need to change DA and offerId for config although they are id
    @PUT
    @Path("{idbundle}/{DA}/{offerId}")
    @Transactional
    public int updateBundleOfferelementaire(
            @PathParam("idbundle") Integer idbundle,
            @PathParam("DA") Integer DA,
            @PathParam("offerId") Integer offerId,
            BundleOfferelementaire bundleOfferelementaire){

        //1=succes and 0=error

        return BundleOfferelementaire.update("" +
            "UPDATE FROM BundleOfferelementaire SET " +
            "DA= :newDA, " +
            "offerId= :newofferId, " +
            "istoexcludefrominfoconso= :istoexcludefrominfoconso, " +
            "ismixed= :ismixed, " +
            "tarifsmsnat= :tarifsmsnat, " +
            "tarifappelnat= :tarifappelnat, " +
            "tarifdata= :tarifdata, " +
            "tarifdatafb= :tarifdatafb, " +
            "tarifappelfamille= :tarifappelfamille, " +
            "tarifappelorange= :tarifappelorange, " +
            "tarifappelinternational= :tarifappelinternational, " +
            "tarifsmsfamille= :tarifsmsfamille, " +
            "tarifsmsorange= :tarifsmsorange, " +
            "tarifsmsinternational= :tarifsmsinternational " +
            "WHERE idbundle= :idbundle AND DA= :DA AND offerId= :offerId",

            Parameters.with("newDA", bundleOfferelementaire.DA)
                .and("newofferId", bundleOfferelementaire.offerId)
                .and("istoexcludefrominfoconso", bundleOfferelementaire.istoexcludefrominfoconso)
                .and("ismixed", bundleOfferelementaire.ismixed)
                .and("tarifsmsnat", bundleOfferelementaire.tarifsmsnat)
                .and("tarifappelnat", bundleOfferelementaire.tarifappelnat)
                .and("tarifdata", bundleOfferelementaire.tarifdata)
                .and("tarifdatafb", bundleOfferelementaire.tarifdatafb)
                .and("tarifappelfamille", bundleOfferelementaire.tarifappelfamille)
                .and("tarifappelorange", bundleOfferelementaire.tarifappelorange)
                .and("tarifappelinternational", bundleOfferelementaire.tarifappelinternational)
                .and("tarifsmsfamille", bundleOfferelementaire.tarifsmsfamille)
                .and("tarifsmsorange", bundleOfferelementaire.tarifsmsorange)
                .and("tarifsmsinternational", bundleOfferelementaire.tarifsmsinternational)
                .and("idbundle", idbundle)
                .and("DA", DA)
                .and("offerId", offerId));
    }

    //Delete a bundleofferelementaire
    @DELETE
    @Path("{idbundle}/{DA}/{offerId}")
    @Transactional
    public boolean deleteBundleOfferelementaire(
            @PathParam("idbundle") Integer idbundle,
            @PathParam("DA") Integer DA,
            @PathParam("offerId") Integer offerId){
        return BundleOfferelementaire.deleteById(new BundleOfferelementaireId(idbundle,DA,offerId));
    }

}
