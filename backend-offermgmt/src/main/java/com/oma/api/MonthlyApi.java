package com.oma.api;

import com.oma.entity.Monthly;
import com.oma.repository.MonthlyRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/monthly")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MonthlyApi {

    @Inject
    MonthlyRepository monthlyRepository;

    //Get the list of monthly
    @GET
    public List<Monthly> listMonthly(){
        return Monthly.listAll();
    }

    //Add new monthly
    @POST
    @Transactional //use in CRUD operation
    public Response addMonthly(Monthly monthly){
        monthly.persist();
        return Response.status(Response.Status.CREATED).entity(monthly).build();
    }

    //Update the monthly created
    @PUT
    @Path("{id}")
    @Transactional
    public Response updateMonthly(
            @PathParam("id") Integer id, Monthly monthly){
        Monthly mEntity = monthlyRepository.update(id, monthly);
        return Response.ok(mEntity).build();
    }
}
