package com.oma.api;

import com.oma.entity.Otherservice;
import com.oma.repository.OtherserviceRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/otherservice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OtherserviceApi {

    @Inject
    OtherserviceRepository otherserviceRepository;

    //Get the list of otherservice
    @GET
    public List<Otherservice> listOtherservice(){
        return Otherservice.listAll();
    }

    //Add new otherservice
    @POST
    @Transactional //use in CRUD operation
    public Response addOtherservice(Otherservice otherservice){
        otherservice.persist();
        return Response.status(Response.Status.CREATED).entity(otherservice).build();
    }

    //Update the otherservice created
    @PUT
    @Path("{idotherservice}")
    @Transactional
    public Response updateOtherservice(
            @PathParam("idotherservice") Integer idotherservice, Otherservice otherservice){
        Otherservice oEntity = otherserviceRepository.update(idotherservice, otherservice);
        return Response.ok(oEntity).build();
    }
}
