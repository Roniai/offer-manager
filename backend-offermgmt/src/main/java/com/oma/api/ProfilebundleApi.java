package com.oma.api;

import com.oma.entity.Profilebundle;
import com.oma.tablepk.ProfilebundleId;
import io.quarkus.panache.common.Parameters;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/profilebundle")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfilebundleApi {

    @Inject
    EntityManager em;

    //Get all idprofile of a idbundle
    @GET
    @Path("/getidprofile/{idbundle}")
    public List<Profilebundle> getIdprofile(@PathParam("idbundle") Integer idbundle){
        return Profilebundle.find("SELECT p.idprofile FROM Profilebundle p WHERE p.idbundle= :idbundle",
            Parameters.with("idbundle", idbundle)).list();
    }


    //Add new profilebundle
    @POST
    @Path("{idbundle}")
    @Transactional //use in CRUD operation
    public String[] addProfilebundle(@PathParam("idbundle") Integer idbundle,
                                     String[] listIdprofile){
       for(String idprofile:listIdprofile){
           em.createNativeQuery("INSERT INTO profilebundle VALUES(?,?)")
               .setParameter(1, idprofile)
               .setParameter(2, idbundle)
               .executeUpdate();
       }
       return listIdprofile;
    }

    //Delete a profilebundle
    @DELETE
    @Path("{idbundle}/{idprofile}")
    @Transactional
    public boolean deleteProfile(
            @PathParam("idprofile") String idprofile,
            @PathParam("idbundle") Integer idbundle){
        return Profilebundle.deleteById(new ProfilebundleId(idbundle, idprofile));
    }

}
