package com.oma.api;

import com.oma.entity.Profileotherservice;
import com.oma.tablepk.ProfileotherserviceId;
import io.quarkus.panache.common.Parameters;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/profileotherservice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfileotherserviceApi {

    @Inject
    EntityManager em;

    //Get all idprofile of a idotherservice
    @GET
    @Path("/getidprofile/{idotherservice}")
    public List<Profileotherservice> getIdprofile(@PathParam("idotherservice") Integer idotherservice){
        return Profileotherservice.find("SELECT p.idprofile FROM Profileotherservice p WHERE p.idotherservice= :idotherservice",
                Parameters.with("idotherservice", idotherservice)).list();
    }

    //Add new profileotherservice
    @POST
    @Path("{idotherservice}")
    @Transactional //use in CRUD operation
    public String[] addProfileotherservice(@PathParam("idotherservice") Integer idotherservice,
                                        String[] listIdprofile){
        for(String idprofile:listIdprofile){
            em.createNativeQuery("INSERT INTO profileotherservice VALUES(?,?)")
                .setParameter(1, idprofile)
                .setParameter(2, idotherservice)
                .executeUpdate();
        }
        return listIdprofile;
    }

    //Delete a profileotherservice
    @DELETE
    @Path("{idprofile}/{idotherservice}")
    @Transactional
    public boolean deleteProfileotherservice(
            @PathParam("idprofile") String idprofile,
            @PathParam("idotherservice") Integer idotherservice){
        return Profileotherservice.deleteById(new ProfileotherserviceId(idprofile, idotherservice));
    }

}
