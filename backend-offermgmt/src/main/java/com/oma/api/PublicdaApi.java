package com.oma.api;

import com.oma.entity.Publicda;
import com.oma.repository.PublicdaRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/publicda")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PublicdaApi {

    @Inject
    PublicdaRepository publicdaRepository;

    //Get the list of publicda
    @GET
    public List<Publicda> listPublicda(){
        return Publicda.listAll();
    }

    //Add new publicda
    @POST
    @Transactional //use in CRUD operation
    public Response addPublicda(Publicda publicda){
        publicda.persist();
        return Response.status(Response.Status.CREATED).entity(publicda).build();
    }

    //Update the publicda created
    @PUT
    @Path("{idprofile}/{DA}")
    @Transactional
    public Response updateBundleOfferelementaire(
            @PathParam("idprofile") String idprofile,
            @PathParam("DA") Integer DA,
            Publicda publicda){
        Publicda pEntity = publicdaRepository.update(idprofile, DA, publicda);
        return Response.ok(pEntity).build();
    }
}
