package com.oma.api;

import com.oma.entity.Profile;
import com.oma.entity.Unit;
import com.oma.repository.UnitRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/unit")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UnitApi {

    @Inject
    UnitRepository unitRepository;

    //Get the list of unit
    @GET
    public List<Unit> listUnit(){
        return Unit.listAll();
    }

    //Add new unit
    @POST
    @Transactional //use in CRUD operation
    public Response addUnit(Unit unit){
        unit.persist();
        return Response.status(Response.Status.CREATED).entity(unit).build();
    }

    //Update the unit created
    @PUT
    @Path("{unit}")
    @Transactional
    public Response updateUnit(
            @PathParam("unit") Integer id, Unit unit){
        Unit uEntity = unitRepository.update(id, unit);
        return Response.ok(uEntity).build();
    }

    //Delete a unit
    @DELETE
    @Path("{unit}")
    @Transactional
    public boolean deleteUnit(
            @PathParam("unit") Integer id){
        return Unit.deleteById(id);
    }
}
