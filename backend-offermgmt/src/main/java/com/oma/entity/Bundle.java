package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
public class Bundle extends PanacheEntityBase {
    @Id
    public Integer idbundle;

    //Marketing level
    public String nom;
    public Integer isBuyableForOther;
    public Integer isautorenewable;
    public Integer 	isbuylimited;
    public Integer buymaximum;
    public String buyfrequency;
    public String description;
    public float prix;
    public String taxe;
    public String validite;
    public Integer isBuyableByOM;
    public Integer isBuyableWithLanyCredit;
    public String groups;
    public Integer grouprank;
    public String groups1;
    public Integer groupsrank1;
    public String groups2;
    public Integer groupsrank2;
    public String groups3;
    public Integer groupsrank3;
    public String groups4;
    public Integer groupsrank4;
    public String groups5;
    public Integer groupsrank5;
    public Integer generalrank;
    public String commentaire;

    //Config level
    public Integer isroaming;
    public Integer isActive;
    public String refill;
    public String refillinjection;
    public String refillfwd;
    public String refillapi;
    public String refilllms;
    public String refillmypos;
    public Integer chargingindicator;
    public Integer chargingindicatorom;
    public Integer pamclass;
    public Integer pamservice;
    public Integer pamscheduled;
    public Integer accumulatorid;
    public String refillom;
    public String refilllanycredit;

    public Integer getIdbundle() {
        return idbundle;
    }

    public Integer getIsroaming() {
        return isroaming;
    }

    public void setIsroaming(Integer isroaming) {
        this.isroaming = isroaming;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getRefill() {
        return refill;
    }

    public void setRefill(String refill) {
        this.refill = refill;
    }

    public String getRefillinjection() {
        return refillinjection;
    }

    public void setRefillinjection(String refillinjection) {
        this.refillinjection = refillinjection;
    }

    public String getRefillfwd() {
        return refillfwd;
    }

    public void setRefillfwd(String refillfwd) {
        this.refillfwd = refillfwd;
    }

    public String getRefillapi() {
        return refillapi;
    }

    public void setRefillapi(String refillapi) {
        this.refillapi = refillapi;
    }

    public String getRefilllms() {
        return refilllms;
    }

    public void setRefilllms(String refilllms) {
        this.refilllms = refilllms;
    }

    public String getRefillmypos() {
        return refillmypos;
    }

    public void setRefillmypos(String refillmypos) {
        this.refillmypos = refillmypos;
    }

    public Integer getChargingindicator() {
        return chargingindicator;
    }

    public void setChargingindicator(Integer chargingindicator) {
        this.chargingindicator = chargingindicator;
    }

    public Integer getChargingindicatorom() {
        return chargingindicatorom;
    }

    public void setChargingindicatorom(Integer chargingindicatorom) {
        this.chargingindicatorom = chargingindicatorom;
    }

    public Integer getPamclass() {
        return pamclass;
    }

    public void setPamclass(Integer pamclass) {
        this.pamclass = pamclass;
    }

    public Integer getPamservice() {
        return pamservice;
    }

    public void setPamservice(Integer pamservice) {
        this.pamservice = pamservice;
    }

    public Integer getPamscheduled() {
        return pamscheduled;
    }

    public void setPamscheduled(Integer pamscheduled) {
        this.pamscheduled = pamscheduled;
    }

    public Integer getAccumulatorid() {
        return accumulatorid;
    }

    public void setAccumulatorid(Integer accumulatorid) {
        this.accumulatorid = accumulatorid;
    }

    public String getRefillom() {
        return refillom;
    }

    public void setRefillom(String refillom) {
        this.refillom = refillom;
    }

    public String getRefilllanycredit() {
        return refilllanycredit;
    }

    public void setRefilllanycredit(String refilllanycredit) {
        this.refilllanycredit = refilllanycredit;
    }
}
