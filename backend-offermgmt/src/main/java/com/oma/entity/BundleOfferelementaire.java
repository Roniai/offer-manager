package com.oma.entity;

import com.oma.tablepk.BundleOfferelementaireId;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
//The IdClass annotation maps multiple fields to the table PK (PrimaryKey).
@IdClass(BundleOfferelementaireId.class)
public class BundleOfferelementaire extends PanacheEntityBase{
    @Id
    public Integer idbundle;
    @Id
    public Integer DA;
    @Id
    public Integer offerId;

    public Long quantity;
    public Integer istoexcludefrominfoconso;
    public Integer ismixed;
    public float tarifsmsnat;
    public float tarifappelnat;
    public float tarifdata;
    public float tarifdatafb;
    public float tarifappelfamille;
    public float tarifappelorange;
    public float tarifappelinternational;
    public float tarifsmsfamille;
    public float tarifsmsorange;
    public float tarifsmsinternational;

}
