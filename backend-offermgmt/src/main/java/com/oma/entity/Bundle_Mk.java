package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
//Name of table in BD
@Table(name="bundle")
public class Bundle_Mk extends PanacheEntityBase{
    /*We use Integer istead of int
    cause it can assign NULL value property*/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer idbundle;

    public String nom;
    public Integer isBuyableForOther;
    public Integer isautorenewable;
    public Integer 	isbuylimited;
    public Integer buymaximum;
    public String buyfrequency;
    public String description;
    public float prix;
    public String taxe;
    public String validite;
    public Integer isBuyableByOM;
    public Integer isBuyableWithLanyCredit;
    public String groups;
    public Integer grouprank;
    public String groups1;
    public Integer groupsrank1;
    public String groups2;
    public Integer groupsrank2;
    public String groups3;
    public Integer groupsrank3;
    public String groups4;
    public Integer groupsrank4;
    public String groups5;
    public Integer groupsrank5;
    public Integer generalrank;
    public String commentaire;
}
