package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Monthly extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    public String nom;
    public Integer da;
    public Integer offerID;
    public Integer isActive;
    public String groups;
    public Integer groupsrank;
    public String groups1;
    public Integer groupsrank1;
    public String groups2;
    public Integer groupsrank2;
    public String groups3;
    public Integer groupsrank3;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getDa() {
        return da;
    }

    public void setDa(Integer da) {
        this.da = da;
    }

    public Integer getOfferID() {
        return offerID;
    }

    public void setOfferID(Integer offerID) {
        this.offerID = offerID;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public Integer getGroupsrank() {
        return groupsrank;
    }

    public void setGroupsrank(Integer groupsrank) {
        this.groupsrank = groupsrank;
    }

    public String getGroups1() {
        return groups1;
    }

    public void setGroups1(String groups1) {
        this.groups1 = groups1;
    }

    public Integer getGroupsrank1() {
        return groupsrank1;
    }

    public void setGroupsrank1(Integer groupsrank1) {
        this.groupsrank1 = groupsrank1;
    }

    public String getGroups2() {
        return groups2;
    }

    public void setGroups2(String groups2) {
        this.groups2 = groups2;
    }

    public Integer getGroupsrank2() {
        return groupsrank2;
    }

    public void setGroupsrank2(Integer groupsrank2) {
        this.groupsrank2 = groupsrank2;
    }

    public String getGroups3() {
        return groups3;
    }

    public void setGroups3(String groups3) {
        this.groups3 = groups3;
    }

    public Integer getGroupsrank3() {
        return groupsrank3;
    }

    public void setGroupsrank3(Integer groupsrank3) {
        this.groupsrank3 = groupsrank3;
    }
}
