package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
public class Otherservice extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer idotherservice;

    public String refill;
    public String nom;
    public String description;
    public float prix;
    public String taxe;
    public Integer isActive;
    public String validite;
    public String groups;
    public Integer grouprank;
    public String groups1;
    public Integer groupsrank1;
    public String groups2;
    public Integer groupsrank2;
    public String groups3;
    public Integer groupsrank3;
    public Integer orderotherservice;
    public String commentaire;
    public Integer groupsrank;

    public String getRefill() {
        return refill;
    }

    public void setRefill(String refill) {
        this.refill = refill;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getTaxe() {
        return taxe;
    }

    public void setTaxe(String taxe) {
        this.taxe = taxe;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getValidite() {
        return validite;
    }

    public void setValidite(String validite) {
        this.validite = validite;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public Integer getGrouprank() {
        return grouprank;
    }

    public void setGrouprank(Integer grouprank) {
        this.grouprank = grouprank;
    }

    public String getGroups1() {
        return groups1;
    }

    public void setGroups1(String groups1) {
        this.groups1 = groups1;
    }

    public Integer getGroupsrank1() {
        return groupsrank1;
    }

    public void setGroupsrank1(Integer groupsrank1) {
        this.groupsrank1 = groupsrank1;
    }

    public String getGroups2() {
        return groups2;
    }

    public void setGroups2(String groups2) {
        this.groups2 = groups2;
    }

    public Integer getGroupsrank2() {
        return groupsrank2;
    }

    public void setGroupsrank2(Integer groupsrank2) {
        this.groupsrank2 = groupsrank2;
    }

    public String getGroups3() {
        return groups3;
    }

    public void setGroups3(String groups3) {
        this.groups3 = groups3;
    }

    public Integer getGroupsrank3() {
        return groupsrank3;
    }

    public void setGroupsrank3(Integer groupsrank3) {
        this.groupsrank3 = groupsrank3;
    }

    public Integer getOrderotherservice() {
        return orderotherservice;
    }

    public void setOrderotherservice(Integer orderotherservice) {
        this.orderotherservice = orderotherservice;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Integer getGroupsrank() {
        return groupsrank;
    }

    public void setGroupsrank(Integer groupsrank) {
        this.groupsrank = groupsrank;
    }
}
