package com.oma.entity;

import com.oma.tablepk.ProfilebundleId;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
//The IdClass annotation maps multiple fields to the table PK (PrimaryKey).
@IdClass(ProfilebundleId.class)
public class Profilebundle extends PanacheEntityBase {
    @Id
    public Integer idbundle;
    @Id
    public String idprofile;
}
