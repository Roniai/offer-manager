package com.oma.repository;

import com.oma.entity.Bundle;

import javax.enterprise.context.ApplicationScoped;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class BundleRepository{
    public Bundle update(Integer id, Bundle bundle){

        Bundle bundleUpdated = Bundle.findById(id);

        if (bundleUpdated == null) {
            throw new WebApplicationException("The bundle : " + id + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the id
        bundleUpdated.setIsroaming(bundle.getIsroaming());
        bundleUpdated.setIsActive(bundle.getIsActive());
        bundleUpdated.setRefill(bundle.getRefill());
        bundleUpdated.setRefillinjection(bundle.getRefillinjection());
        bundleUpdated.setRefillfwd(bundle.getRefillfwd());
        bundleUpdated.setRefillapi(bundle.getRefillapi());
        bundleUpdated.setRefilllms(bundle.getRefilllms());
        bundleUpdated.setRefillmypos(bundle.getRefillmypos());
        bundleUpdated.setChargingindicator(bundle.getChargingindicator());
        bundleUpdated.setChargingindicatorom(bundle.getChargingindicatorom());
        bundleUpdated.setPamclass(bundle.getPamclass());
        bundleUpdated.setPamservice(bundle.getPamservice());
        bundleUpdated.setPamscheduled(bundle.getPamscheduled());
        bundleUpdated.setAccumulatorid(bundle.getAccumulatorid());
        bundleUpdated.setRefillom(bundle.getRefillom());
        bundleUpdated.setRefilllanycredit(bundle.getRefilllanycredit());

        return bundleUpdated;
    }
}
