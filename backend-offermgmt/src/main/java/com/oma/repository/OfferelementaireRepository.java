package com.oma.repository;

import com.oma.entity.Offerelementaire;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import java.util.List;

@ApplicationScoped
public class OfferelementaireRepository {
    public Offerelementaire update(Integer DA, Offerelementaire offerelementaire){

        Offerelementaire offerelementaireUpdated = Offerelementaire.findById(DA);

        if (offerelementaireUpdated == null) {
            throw new WebApplicationException("The DA : " + DA + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the DA
        offerelementaireUpdated.setTypeInfoConso(offerelementaire.getTypeInfoConso());
        offerelementaireUpdated.setNature(offerelementaire.getNature());
        offerelementaireUpdated.setTypeOffer(offerelementaire.getTypeOffer());
        offerelementaireUpdated.setUnit(offerelementaire.getUnit());

        return offerelementaireUpdated;
    }

    public List<Offerelementaire> listNature() {
        return Offerelementaire.list("SELECT DISTINCT(o.nature) FROM Offerelementaire o",
                Sort.by("o.nature"));
    }

    public List<Offerelementaire> getDANature(String nature) {
        return Offerelementaire.find("SELECT o FROM Offerelementaire o WHERE o.nature= :nature",
                Parameters.with("nature", nature)).list();
    }
}
