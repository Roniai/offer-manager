package com.oma.repository;

import com.oma.entity.Otherservice;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class OtherserviceRepository {
    public Otherservice update(Integer idotherservice, Otherservice otherservice){

        Otherservice otherserviceUpdated = Otherservice.findById(idotherservice);

        if (otherserviceUpdated == null) {
            throw new WebApplicationException("The idotherservice : " + idotherservice + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the idotherservice
        otherserviceUpdated.setRefill(otherservice.getRefill());
        otherserviceUpdated.setNom(otherservice.getNom());
        otherserviceUpdated.setDescription(otherservice.getDescription());
        otherserviceUpdated.setPrix(otherservice.getPrix());
        otherserviceUpdated.setTaxe(otherservice.getTaxe());
        otherserviceUpdated.setIsActive(otherservice.getIsActive());
        otherserviceUpdated.setValidite(otherservice.getValidite());
        otherserviceUpdated.setGroups(otherservice.getGroups());
        otherserviceUpdated.setGrouprank(otherservice.getGrouprank());
        otherserviceUpdated.setGroups1(otherservice.getGroups1());
        otherserviceUpdated.setGroupsrank1(otherservice.getGroupsrank1());
        otherserviceUpdated.setGroups2(otherservice.getGroups2());
        otherserviceUpdated.setGroupsrank2(otherservice.getGroupsrank2());
        otherserviceUpdated.setGroups3(otherservice.getGroups3());
        otherserviceUpdated.setGroupsrank3(otherservice.getGroupsrank3());
        otherserviceUpdated.setOrderotherservice(otherservice.getOrderotherservice());
        otherserviceUpdated.setCommentaire(otherservice.getCommentaire());
        otherserviceUpdated.setGroupsrank(otherservice.getGroupsrank());

        return otherserviceUpdated;
    }
}
