package com.oma.tablepk;

import java.io.Serializable;

public class BundleOfferelementaireId implements Serializable {
    public Integer idbundle;
    public Integer DA;
    public Integer offerId;

    public BundleOfferelementaireId() {
    }

    public BundleOfferelementaireId(Integer idbundle, Integer DA, Integer offerId) {
        this.idbundle = idbundle;
        this.DA = DA;
        this.offerId = offerId;
    }
}
