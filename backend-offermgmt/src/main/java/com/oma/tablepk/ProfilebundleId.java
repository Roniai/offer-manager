package com.oma.tablepk;

import java.io.Serializable;

public class ProfilebundleId implements Serializable {
    public Integer idbundle;
    public String idprofile;

    public ProfilebundleId() {
    }

    public ProfilebundleId(Integer idbundle, String idprofile) {
        this.idbundle = idbundle;
        this.idprofile = idprofile;
    }
}
